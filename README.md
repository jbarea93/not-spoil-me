# Not Spoil me

Elimina las miniaturas de YouTube a partir de una coincidencia del título con palabras clave.

# How To Use

1- Descargar extensión Run Javascript para Chrome desde: https://chrome.google.com/webstore/detail/custom-javascript-for-web/poakhlngfciodnhlhhgnaaelnpjljija

2- Acceder a youtube y clickear sobre el icono de la extensión

3- Marcar el checkbox de 'enable on www.youtube.com'

4- Copiar el contenido de main.js en el cuadro de texto y pulsar en Save & Run

5- Para comprobar la cantidad de video que va eliminando abrir las herramientas de desarrollador y mirar la consola de Javascript

# Funcionamiento

El script se encarga de analizar todas las miniaturas de youtube de forma inteligente, extrayendo el título del video y comprobando si aparece alguna de las palabras clave en el mismo.

En caso de encontrar coincidencia borra automaticamente el elemento de video (miniatura y titulo).

Este proceso se desarrolla cada segundo de forma inteligente analizando la cantidad de miniaturas cargadas y solo se ejecuta si dicho número ha cambiado; Es decir se ejecuta constantemente pero no hace nada si no se han cargado miniaturas nuevas que analizar.