
let miniaturasLength;
function notSpoilMe() {
    let tags=["ytd-playlist-renderer","ytd-grid-video-renderer","ytd-compact-video-renderer","ytd-video-renderer"]
    let miniaturas=[];
    tags.forEach(tag => {
        let elem=Array.from(document.getElementsByTagName(tag))
        miniaturas=miniaturas.concat(elem)
    })

    if (miniaturas.length != miniaturasLength) {
        miniaturasLength=miniaturas.length
        let words=["kingdom","hearts"]
        let deleted=0;
        var videoTitle=""

        for (let i = 0; i < miniaturas.length; i++) {
            const e = miniaturas[i];
            titlesA=Array.from(e.getElementsByTagName("a"))
            titlesSPAN=Array.from(e.getElementsByTagName("span"))
            titles=titlesA.concat(titlesSPAN)
            // Get video title
            for (let t=0; t < titles.length; t++) {
                let title=titles[t].id
                if (title == 'video-title') {
                    videoTitle=titles[t].innerText;
                    break;
                }
            }

            // Check words
            for (let c=0; c < words.length; c++) {
                let w=words[c];
                if (videoTitle.toLowerCase().indexOf(w.toLowerCase()) >= 0) {
                    e.remove()
                    deleted++;
                    break;
                } 
            }
        }
        console.log(`${new Date().toLocaleTimeString()} - Deleted ${deleted} spoilers!`)    
    }
}   
    
notSpoilMe()
var KHInterval=setInterval(function(){
    notSpoilMe()
},1000)